package study.complex;

import org.junit.Test;

import static org.junit.Assert.*;

public class ComplexNumberTest {

    @Test
    public void add() {
        ComplexNumber number1 = new ComplexNumber(3, 2);
        ComplexNumber number2 = new ComplexNumber(5, 11);
        ComplexNumber result = number1.add(number2);
        assertEquals(8, result.real, 0.00001);
        assertEquals(13, result.img, 0.00001);
    }

    @Test
    public void sub() {
        ComplexNumber number1 = new ComplexNumber(12, 8);
        ComplexNumber number2 = new ComplexNumber(14, 12);
        ComplexNumber result = number1.sub(number2);
        assertEquals(-2, result.real, 0.00001);
        assertEquals(-4, result.img, 0.00001);
    }

    @Test
    public void mul() {
        ComplexNumber number1 = new ComplexNumber(5, 6);
        ComplexNumber number2 = new ComplexNumber(3, 7);
        ComplexNumber result = number1.mul(number2);
        assertEquals(-27, result.real, 0.00001);
        assertEquals(53, result.img, 0.00001);
        number2 = new ComplexNumber(0, 3);
        result = number1.mul(number2);
        assertEquals(-18, result.real, 0.00001);
        assertEquals(15, result.img, 0.00001);
    }

    @Test
    public void div() throws ArithmeticException {
        ComplexNumber number1 = new ComplexNumber(12, 4);
        ComplexNumber number2 = new ComplexNumber(4, 2);
        ComplexNumber result = number1.div(number2);
        assertEquals(2.8, result.real, 0.00001);
        assertEquals(-0.4, result.img, 0.00001);
    }

    @Test(expected = ArithmeticException.class)
    public void divByZero() throws ArithmeticException {
        ComplexNumber number1 = new ComplexNumber(12, 4);
        ComplexNumber number2 = new ComplexNumber(0, 0);
        number1.div(number2);
    }

    @Test
    public void pow() {
        ComplexNumber number = new ComplexNumber(4, 11);
        ComplexNumber result = number.pow(3);
        assertEquals(-1388, result.real, 0.00001);
        assertEquals(-803, result.img, 0.00001);
    }

    @Test
    public void pow2() {
        ComplexNumber number = new ComplexNumber(0, 2);
        ComplexNumber result = number.pow(3);
        assertEquals(0, result.real, 0.00001);
        assertEquals(-8, result.img, 0.00001);
    }

    @Test(expected = ArithmeticException.class)
    public void pow0to0() throws ArithmeticException {
        ComplexNumber number = new ComplexNumber(0, 0);
        number.pow(0);
    }

    @Test(expected = ArithmeticException.class)
    public void powZeroToNegative() {
        ComplexNumber number = new ComplexNumber(0, 0);
        number.pow(-1);
    }

    @Test
    public void turnToAngle() {
        ComplexNumber number = new ComplexNumber(10.39230484, 6);
        ComplexNumber result = number.turnToAngle(30);
        assertEquals(6, result.real, 0.00001);
        assertEquals(10.39230484, result.img, 0.00001);
    }

    @Test
    public void turnToAngle2() {
        ComplexNumber number = new ComplexNumber(1, 0);
        ComplexNumber result = number.turnToAngle(-90);
        assertEquals(0, result.real, 0.00001);
        assertEquals(-1, result.img, 0.00001);
    }

    @Test
    public void equals() {
        ComplexNumber number1 = new ComplexNumber(3, 2);
        ComplexNumber number2 = new ComplexNumber(4, 6);
        ComplexNumber number3 = new ComplexNumber(3, 2);
        boolean equals = number1.equals(number3);
        assertTrue(equals);
        boolean notEquals = number1.equals(number2);
        assertNotEquals(true, notEquals);
    }
}