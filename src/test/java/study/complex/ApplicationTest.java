package study.complex;


import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class ApplicationTest {

    @Test
    public void testEquals() throws Exception {
        assertEquals(1, 1);
    }

    @Test
    public void testNotEquals() throws Exception {
        assertNotEquals(2, 1);
    }
}
