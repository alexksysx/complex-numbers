package study.complex.Commands;

import study.complex.Command;
import study.complex.ComplexNumber;
import study.complex.ComplexNumbersMap;

public class PowCommand implements Command {
    private final ComplexNumbersMap map = ComplexNumbersMap.getInstance();

    @Override
    public String getName() {
        return "pow";
    }

    @Override
    public String getDescription() {
        return "raise number to the power of some other number";
    }

    @Override
    public void execute(String[] args) {
        if (args.length != 3) {
            System.out.println("Please provide valid arguments" +
                    "pow VAR NUM");
        }
        if (!map.contains(args[1])) {
            System.out.println("Can not find variable " + args[1]);
            return;
        }
        ComplexNumber number = map.getByName(args[1]);
        int power;
        try {
            power = Integer.valueOf(args[2]);
        } catch (NumberFormatException e) {
            System.out.println(e);
            System.out.println("expected int type");
            return;
        }
        ComplexNumber result = number.pow(power);
        map.saveToResult(result);
    }
}
