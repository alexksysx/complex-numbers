package study.complex.Commands;

import study.complex.Command;
import study.complex.ComplexNumber;
import study.complex.ComplexNumbersMap;

public class UpdateCommand implements Command {
    private final ComplexNumbersMap map = ComplexNumbersMap.getInstance();

    @Override
    public String getName() {
        return "update";
    }

    @Override
    public String getDescription() {
        return "update var";
    }

    @Override
    public void execute(String[] args) {
        if (args.length != 4) {
            System.out.println("Please provide valid arguments:" +
                    "update NAME REAL IMAGINARY");
            return;
        }
        if (!map.contains(args[1])) {
            System.out.println("Can not find variable " + args[1]);
            return;
        }
        double real, img;
        try {

            real = Double.valueOf(args[2]);
            img = Double.valueOf(args[3]);
        } catch (NumberFormatException ex) {
            System.out.println("Please provide arguments of the correct types:" +
                    "update STRING DOUBLE DOUBLE");
            return;
        }
        ComplexNumber number = new ComplexNumber(real, img);
        map.update(args[1], number);
    }
}
