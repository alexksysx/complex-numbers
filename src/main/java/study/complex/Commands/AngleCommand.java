package study.complex.Commands;

import study.complex.Command;
import study.complex.ComplexNumber;
import study.complex.ComplexNumbersMap;

public class AngleCommand implements Command {
    private final ComplexNumbersMap map = ComplexNumbersMap.getInstance();

    @Override
    public String getName() {
        return "angle";
    }

    @Override
    public String getDescription() {
        return "change complex number angle";
    }

    @Override
    public void execute(String[] args) {
        if (args.length != 3) {
            System.out.println("Please provide valid arguments:" +
                    "angle VAR ANGLE");
            return;
        }
        if (!map.contains(args[1])) {
            System.out.println("Can not find variable " + args[1]);
            return;
        }
        ComplexNumber number = map.getByName(args[1]);
        double angle;
        try {
            angle = Double.valueOf(args[2]);
        } catch (NumberFormatException e) {
            System.out.println(e);
            System.out.println("expected double type");
            return;
        }
        ComplexNumber result = number.turnToAngle(angle);
        map.saveToResult(result);
    }
}
