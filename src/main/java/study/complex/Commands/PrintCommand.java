package study.complex.Commands;

import study.complex.Command;
import study.complex.ComplexNumbersMap;

public class PrintCommand implements Command {
    private final ComplexNumbersMap map = ComplexNumbersMap.getInstance();

    @Override
    public String getName() {
        return "print";
    }

    @Override
    public String getDescription() {
        return "print variables";
    }

    @Override
    public void execute(String[] args) {
        if (args.length == 1) {
            String result = map.giveAllNumbers();
            System.out.print(result);
            return;
        }
        if (args.length > 1)
        {
            for (int i = 1; i < args.length; i++) {
                if (map.contains(args[i])) {
                    System.out.println(args[i] + " " + map.getByName(args[i]));
                } else {
                    System.out.println("Can not find variable " + args[i]);
                }
            }
        }
    }
}
