package study.complex.Commands;

import study.complex.Command;
import study.complex.ComplexNumbersMap;

public class CreateCommand implements Command {
    private final ComplexNumbersMap map = ComplexNumbersMap.getInstance();

    @Override
    public String getName() {
        return "create";
    }

    @Override
    public String getDescription() {
        return "create new complex number variable";
    }

    @Override
    public void execute(String[] args) {
        if (args.length != 4) {
            System.out.println("Please provide some arguments:" +
                    "create NAME REAL IMAGINARY");
            return;
        }
        if (map.contains(args[1])) {
            System.out.println("Number with this name already exist");
            return;
        }
        double real;
        double img;
        try {

            real = Double.valueOf(args[2]);
            img = Double.valueOf(args[3]);
        } catch (NumberFormatException ex) {
            System.out.println("Please provide arguments of the correct types:" +
                    "create STRING DOUBLE DOUBLE");
            return;
        }
        map.createNumber(args[1], real, img);
    }
}
