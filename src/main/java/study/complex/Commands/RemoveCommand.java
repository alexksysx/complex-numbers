package study.complex.Commands;

import study.complex.Command;
import study.complex.ComplexNumbersMap;

public class RemoveCommand implements Command {
    private final ComplexNumbersMap map = ComplexNumbersMap.getInstance();

    @Override
    public String getName() {
        return "remove";
    }

    @Override
    public String getDescription() {
        return "remove complex number variable";
    }

    @Override
    public void execute(String[] args) {
        if (args.length != 2) {
            System.out.println("Please provide some arguments:" +
                    "remove NAME");
            return;
        }
        if (map.contains(args[1])) {
            map.remove(args[1]);
        }
    }
}
