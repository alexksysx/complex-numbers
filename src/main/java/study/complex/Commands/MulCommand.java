package study.complex.Commands;

import study.complex.Command;
import study.complex.ComplexNumber;
import study.complex.ComplexNumbersMap;

public class MulCommand implements Command {
    private final ComplexNumbersMap map = ComplexNumbersMap.getInstance();

    @Override
    public String getName() {
        return "mul";
    }

    @Override
    public String getDescription() {
        return "multiply 2 numbers";
    }

    @Override
    public void execute(String[] args) {
        if (args.length != 3) {
            System.out.println("Please provide valid arguments:" +
                    "mul VAR VAR");
            return;
        }
        if (!map.contains(args[1])) {
            System.out.println("Can not find variable " + args[1]);
            return;
        }
        if (!map.contains(args[2])) {
            System.out.println("Can not find variable " + args[2]);
            return;
        }
        ComplexNumber number1 = map.getByName(args[1]);
        ComplexNumber number2 = map.getByName(args[2]);
        ComplexNumber result = number1.mul(number2);
        map.saveToResult(result);
    }
}
