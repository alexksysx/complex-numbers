package study.complex;

import study.complex.Commands.*;

import java.io.IOException;
import java.io.PrintStream;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class ShellService {

    private PrintStream outputStream = System.out;

    private boolean terminate;
    private Map<String, Command> commands = new LinkedHashMap<>();

    public void setCommands(List<Command> commandsList) {
        for (Command command : commandsList) {
            addCommand(command);
        }
    }

    private class ExitCommand implements Command {

        public String getName() {
            return "exit";
        }

        public String getDescription() {
            return "Exit application.";
        }

        public void execute(String[] args) {
            terminate = true;
            outputStream.println("Good bye!");
        }
    }

    private class HelpCommand implements Command {

        public String getName() {
            return "help";
        }

        public String getDescription() {
            return "Display help information.";
        }

        public void execute(String[] args) {
            for (Command command : commands.values()) {
                outputStream.println(command.getName() + " - " + command.getDescription());
            }
        }
    }

    public ShellService() {
        addCommand(new HelpCommand());
        addCommand(new ExitCommand());
        addCommand(new CreateCommand());
        addCommand(new RemoveCommand());
        addCommand(new PrintCommand());
        addCommand(new AddCommand());
        addCommand(new SubCommand());
        addCommand(new UpdateCommand());
        addCommand(new MulCommand());
        addCommand(new DivCommand());
        addCommand(new PowCommand());
        addCommand(new AngleCommand());
        addCommand(new EqualsCommand());
    }

    public void addCommand(Command command) {
        commands.put(command.getName(), command);
    }

    public void run() throws IOException {
        outputStream.println();
        outputStream.println("You can use 'help':");

        while (!terminate) {
            outputStream.print("> ");

            Scanner scanner = new Scanner(System.in);
            String line = scanner.nextLine();
            String[] args = line.split(" ");
            if (args.length > 0) {
                if (commands.containsKey(args[0])) {
                    Command command = commands.get(args[0]);
                    command.execute(args);
                } else {
                    outputStream.println("No such command!");
                }
            }
        }
    }
}
