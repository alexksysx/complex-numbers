package study.complex;

public class ComplexNumber {
    double real, img;

    public ComplexNumber(double real, double img) {
        this.real = real;
        this.img = img;
    }

    public ComplexNumber add(ComplexNumber number) {
        double real = this.real + number.real;
        double img = this.img + number.img;
        return new ComplexNumber(real, img);
    }

    public ComplexNumber sub(ComplexNumber number) {
        double real = this.real - number.real;
        double img = this.img - number.img;
        return new ComplexNumber(real, img);
    }

    public ComplexNumber mul(ComplexNumber number) {
        double real = this.real * number.real - this.img * number.img;
        double img = this.img * number.real + this.real * number.img;
        return new ComplexNumber(real, img);
    }

    public ComplexNumber div(ComplexNumber number) throws ArithmeticException {
        Double real = (this.real * number.real + this.img * number.img) / (Math.pow(number.real, 2) + Math.pow(number.img, 2));
        Double img = (this.img * number.real - this.real * number.img) / (Math.pow(number.real, 2) + Math.pow(number.img, 2));
        if ((real.isInfinite()) || (real.isNaN())) {
            throw new ArithmeticException("Divide by zero");
        }
        return new ComplexNumber(real, img);
    }

    public ComplexNumber pow(int exponent) {
        if (exponent == 0) {
            if ((real == 0) && (img == 0))
                throw new ArithmeticException("Uncertainty in 0^0");
            return new ComplexNumber(1, 0);
        }
        if ((exponent < 0) && (real == 0) && (img == 0)) {
            throw new ArithmeticException("Can not power zero to negative number");
        }
        double module = Math.sqrt(Math.pow(real, 2) + Math.pow(img, 2));
        double argument = Math.atan(img / real);
        module = Math.pow(module, exponent);
        argument *= exponent;
        double treal = module * Math.cos(argument);
        double timg = module * Math.sin(argument);
        return new ComplexNumber(treal, timg);
    }

    public boolean equals(ComplexNumber number) {
        return (Math.abs(real - number.real) < 0.00001) && (Math.abs(img - number.img) < 0.00001);
    }

    public ComplexNumber turnToAngle(double angle) {
        double module = Math.sqrt(Math.pow(real, 2) + Math.pow(img, 2));
        double argument = Math.atan(img / real);
        double rad = Math.toRadians(angle);
        argument += rad;
        double real = module * Math.cos(argument);
        double img = module * Math.sin(argument);
        return new ComplexNumber(real, img);
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append(real);
        if ((img > 0) && (img != 0))
            str.append("+" + img + "i");
        else if ((img < 0) && (img != 0))
            str.append(img + "i");
        return str.toString();
    }
}
