package study.complex;

import java.util.*;

import java.util.HashMap;
import java.util.function.BiFunction;

public class ComplexNumbersMap {
    private static ComplexNumbersMap ourInstance = new ComplexNumbersMap();
    Map<String, ComplexNumber> numbers = new HashMap<>();

    public static ComplexNumbersMap getInstance() {
        return ourInstance;
    }

    private ComplexNumbersMap() {
        ComplexNumber result = new ComplexNumber(0, 0);
        numbers.put("result", result);
    }

    public void createNumber(String name, double real, double img) {
        ComplexNumber number = new ComplexNumber(real, img);
        numbers.put(name, number);
    }

    public ComplexNumber getByName(String name) {
        return numbers.get(name);
    }

    public void remove(String name) {
        numbers.remove(name);
    }

    public boolean contains(String name) {
        if (numbers.containsKey(name))
            return true;
        else
            return false;
    }

    public void update(String name, ComplexNumber number) {
        numbers.remove(name);
        numbers.put(name, number);
    }

    public String giveAllNumbers() {
        StringBuilder builder = new StringBuilder();
        for(Map.Entry<String, ComplexNumber> number : numbers.entrySet()) {
            builder.append(number.getKey() + " " + number.getValue().toString() + "\n");
        }
        return builder.toString();
    }

    public void saveToResult(ComplexNumber result) {
        numbers.remove("result");
        numbers.put("result", result);
    }
}
